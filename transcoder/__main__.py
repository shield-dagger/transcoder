from sys import argv

try:
    if argv[1] == "daemon":
        from . import daemon
        try:
            daemon.__dict__[argv[2]](*argv[3:])
        except (KeyError, TypeError) as e:
            if type(e) == TypeError:
                print("Missing arguments.")
            else:
                print(argv[2], "is not a valid daemon runtime mode.")
            print("Use one of the following:")
            for key in daemon.__dict__.keys():
                if not key.startswith("_"):
                    print(" "*4+key)
                    if daemon.__dict__[key].__code__.co_argcount:
                        print(" "*4+ " Args:")
                    for arg in daemon.__dict__[key].__code__.co_varnames[:daemon.__dict__[key].__code__.co_argcount]:
                        print(" "*8+"- "+arg)
except KeyboardInterrupt:
    exit(0)