import logging
from logging import root

logging.basicConfig(
    format="%(asctime)s [%(name)s] - %(levelname)s: %(message)s",
    datefmt=r"%Y-%m-%d %H:%M:%S",
    level=logging.DEBUG
)