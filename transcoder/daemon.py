from . import root as _root
from os import getenv as _getenv

_REPLICATOR_PORT = 25585
_RESOLUTIONS = [1080, 900, 720, 480, 360]
_TCODE_FORMAT = "ENC %s %d %d"

_STREAM_URL = _getenv("STREAM_URL", "https://stream.shielddagger.com/stream")


def transcode(replicator_ip):
    _logger = _root.getChild("transcoder")
    import socket
    from time import sleep
    from .encoding import start_transcoder
    from subprocess import Popen
    import ffmpeg
    _logger.info("Connecting to replicator...")
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sck:
            retries = 0
            while True:
                try:
                    sck.connect((replicator_ip, _REPLICATOR_PORT))
                    break
                except ConnectionRefusedError:
                    if retries<10:
                        retries = retries + 1
                        _logger.warn("Controller not online yet. Retries=%d", retries)
                        sleep(2*retries)
                    else:
                        _logger.error("Unable to connect to controller.")
                        raise
            sck.sendall(b"ENC")
            data = b""
            _logger.info("Receiving data.")
            while True:
                pk = sck.recv(1024)
                if not pk:
                    break
                data = data + pk
            _logger.debug("Received: %s", data)
            parts = data.decode("utf-8").split(" ")
            if parts[0] == "ENC":
                _logger.info("Starting transcoder...")
                enc = start_transcoder(parts[1], int(parts[2]), True, parts[3])
                proc = ffmpeg.run_async(enc)
                _logger.info("Encoder ready.")
        proc.communicate()
        _logger.info("Encoder finished.")


class _Controller:
    def __init__(self):
        import atexit
        import signal
        import threading
        self._logger = _root.getChild("controller")
        self._logger.info("Creating controller.")
        self._socket_open = True
        self.resolutions = _RESOLUTIONS
        self.encoders = {}
        self.lock = threading.Condition(threading.Lock())
        atexit.register(self.stop)
        self._logger.info("Initializing controller thread...")
        self.thread = threading.Thread(target=self._controller_thread)
    
    def stop(self):
        self._logger.info("Stopping controller thread...")
        self._socket_open = False
        with self.lock:
            self.lock.notify()
        self.thread.join()
        self._logger.info("Controller thread stopped.")
    
    def start(self):
        self._logger.info("Starting controller thread...")
        self.thread.start()

    def _controller_thread(self):
        import socket
        from time import sleep
        from random import randrange
        self._logger.info("Controller thread started.")
        self._logger.info("Setting up controller socket.")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sck:
            retries = 0
            while True:
                try:
                    sck.bind(("0.0.0.0", _REPLICATOR_PORT))
                    break
                except OSError:
                    if retries < 10:
                        retries = retries+1
                        self._logger.warn("Port binding failed. Retries=%d", retries)
                        sleep(2*retries)
                    else:
                        self._logger.error("Unable to bind to port.")
                        raise


            sck.listen(10)
            while self._socket_open:
                with self.lock:
                    if not len(self.resolutions):
                        self._logger.info("Resolutions assigned. Waiting...")
                        self.lock.notify()
                        self.lock.wait()
                        if not self._socket_open: break
                    self._logger.info("Waiting on transcoder connection...")
                    conn, addr = sck.accept()
                    with conn:
                        self._logger.debug("Received connection from %s", addr)
                        conn.settimeout(2)
                        try:
                            recv = conn.recv(1024)
                        except socket.timeout:
                            self._logger.warn("Failed handshake from %s, ignoring.", addr)
                            continue
                        if recv != b"ENC":
                            self._logger.warn("Failed handshake from %s, ignoring.", addr)
                            continue
                        conn.settimeout(None)
                        res = self.resolutions.pop()
                        port = randrange(49152, 65535)
                        conn.sendall((_TCODE_FORMAT % (
                            _STREAM_URL,
                            res,
                            port
                        )).encode("utf-8"))
                        self.encoders[res] = f"rtmp://{addr[0]}:{port}/app"
                        self._logger.info("Resolution %d assigned to %s", res, self.encoders[res])
        self._logger.info("Controller thread exitting...")


def replicate():
    _logger = _root.getChild("replicator")
    from .encoding import start_splitter
    _logger.info("Starting controller.")
    ctrl = _Controller()
    ctrl.start()
    while True:
        _logger.info("Acquiring lock...")
        with ctrl.lock:
                _logger.info("Waiting on encoders...")
                if len(ctrl.resolutions): ctrl.lock.wait()
                if len(ctrl.resolutions): break
                _logger.info("Starting replicator...")
                _logger.debug(str(list(ctrl.encoders.items())))
                start_splitter(
                    list(ctrl.encoders.items())
                )
                for res in _RESOLUTIONS:
                    ctrl.resolutions.append(res)
                for key in ctrl.encoders:
                    del ctrl.encoders[key]
                ctrl.lock.notify()


def start_all():
    from multiprocessing import Process
    repl = Process(target=replicate)
    repl.start()
    enc = Process(target=transcode, args=("127.0.0.1",))
    enc.start()
    try:
        repl.join()
        enc.join()
    except:
        if repl.is_alive():
            repl.kill()
        if enc.is_alive():
            enc.kill()
        raise