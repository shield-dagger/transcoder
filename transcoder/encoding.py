import socket
from os import getenv
from random import randrange
from shlex import quote
from time import sleep
import ffmpeg
from urllib.parse import urlparse
from . import root

logger = root.getChild("encoding")

RTMP_BUFFER_LEN = 60
RATE_2160 = 29500
RATE_1080 = 6000
RATE_900 = 6000
RATE_720 = 4400
RATE_480 = 1600
RATE_360 = 900
RATE_240 = 600

h264_settings = {
    "crf": 20,
    "sc_threshold": 0,
    "g": 48,
    "keyint_min": 48,
    "profile:v": "main",
    "preset": "ultrafast"
}

hls_settings = {
    "hls_time": 4,
    "hls_playlist_type": "event"
}

resmap = {
    None: {
        "c:a": "copy",
        "c:v": "copy"
    },
    240: {
        "b:v":f"{RATE_240}k",
        "maxrate": f"{int(RATE_240*1.07)}k",
        "bufsize": f"{int(RATE_240*1.5)}k",
        "b:a": "64k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    },
    360: {
        "b:v":f"{RATE_360}k",
        "maxrate": f"{int(RATE_360*1.07)}k",
        "bufsize": f"{int(RATE_360*1.5)}k",
        "b:a": "96k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    },
    480:{
        "b:v":f"{RATE_480}k",
        "maxrate": f"{int(RATE_480*1.07)}k",
        "bufsize": f"{int(RATE_480*1.5)}k",
        "b:a": "128k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    },
    720:{
        "b:v":f"{RATE_720}k",
        "maxrate": f"{int(RATE_720*1.07)}k",
        "bufsize": f"{int(RATE_720*1.5)}k",
        "b:a": "128k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    },
    900:{
        "b:v":f"{RATE_900}k",
        "maxrate": f"{int(RATE_900*1.07)}k",
        "bufsize": f"{int(RATE_900*1.5)}k",
        "b:a": "192k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    },
    1080:{
        "b:v":f"{RATE_1080}k",
        "maxrate": f"{int(RATE_1080*1.07)}k",
        "bufsize": f"{int(RATE_1080*1.5)}k",
        "b:a": "192k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    },
    2160:{
        "b:v":f"{RATE_2160}k",
        "maxrate": f"{int(RATE_2160*1.07)}k",
        "bufsize": f"{int(RATE_2160*1.5)}k",
        "b:a": "192k",
        "ar": "48000",
        "c:a": "aac",
        "c:v": "h264"
    }
}

def start_splitter(servers:list):
    splitter = ffmpeg.input("rtmp://0.0.0.0/app", listen=1, hide_banner=None)
    outputs = []
    for res, server in servers:
        outputs.append(ffmpeg.output(splitter, server, vcodec="copy", acodec="copy", f="flv"))
        logger.info("Added duplicator output to %s transcoding for %sp", server, res)
    splitter = ffmpeg.merge_outputs(*outputs)
    logger.debug(" ".join(ffmpeg.compile(splitter)))
    ffmpeg.run(splitter)


def start_transcoder(output_prefix:str, res, construct_only=False, port=None):
    if port is None:
        port = randrange(49152, 65535)
    logger.info("Starting encoder on %s with res %s",
        f"rtmp://0.0.0.0:{port}/app",
        res
    )
    source:ffmpeg.nodes.Stream = ffmpeg.input(
        f"rtmp://0.0.0.0:{port}/",
        rtmp_buffer=RTMP_BUFFER_LEN*1000,
        listen=1,
        hide_banner=None
    )
    scaled = ffmpeg.filter(source, "scale", -2, res, flags="fast_bilinear")
    output = ffmpeg.output(
        scaled, source.audio,
        output_prefix+f"/{res}p.m3u8",
        **resmap[res],
        **h264_settings,
        **hls_settings,
        hls_segment_filename=output_prefix+f"/{res}p_%03d.ts"
    )
    logger.debug(" ".join(ffmpeg.compile(output)))
    if construct_only:
        return output
    ffmpeg.run(output)
